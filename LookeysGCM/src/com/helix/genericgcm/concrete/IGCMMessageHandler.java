package com.helix.genericgcm.concrete;

public interface IGCMMessageHandler {
	//AK: TODO - implement this according to the required messages
	public long handleMessage(String message, VersionUpdateThread updateThread);
}
