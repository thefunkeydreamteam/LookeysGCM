package com.helix.genericgcm.concrete;

import com.helix.genericgcm.protocl.GCMLogic;
import com.helix.genericgcm.protocl.GcmIntentService;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;

public class ServiceStarter extends BroadcastReceiver {
	//private Context mContext;
	
	public String TAG = "Lookeys ServiceStarter";
	
	public void onReceive(Context context, Intent intent) {
		if(Parameters.ENABLE_DEBUG) Log.i(TAG, "received boot intent starting activity - " + intent.getAction());
		
		// LE: Not sure this is needed
		if(intent.getAction().equalsIgnoreCase("com.google.android.c2dm.intent.RECEIVE")){
			ComponentName comp = new ComponentName(context.getPackageName(),
                GcmIntentService.class.getName());
			context.startService(intent.setComponent(comp));
			return;
		}
        
		if(intent.getAction().equalsIgnoreCase("android.net.conn.CONNECTIVITY_CHANGE")){
			SharedPreferences prefs = context.getSharedPreferences(GCMLogic.class.getSimpleName(),
					Context.MODE_PRIVATE);
			SharedPreferences.Editor editor = prefs.edit();
			editor.putBoolean(VersionUpdateThread.DOWNLOAD_LOCK, false);
			editor.commit();
			if(Parameters.ENABLE_DEBUG) 
				Log.w(TAG, "ServiceStarter UnlockDownload");
		}
		Intent serviceIntent = new Intent();		
		serviceIntent.setAction("com.helix.genericgcm.protocl.GCMLogic");
		context.startService(serviceIntent);
	}
}
