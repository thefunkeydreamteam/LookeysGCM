package com.helix.genericgcm.concrete;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import android.util.Log;

public class CommandLinePackageManager {
	private static final String TAG = "LookeysGCM CommandLinePackageManager";
	public String mCommand;
	
	public boolean runCommandLine(String commandLine){
		mCommand = commandLine;
		try {
			
			Process process = Runtime.getRuntime().exec(commandLine);
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(process.getInputStream()));
			String line = null;
			do{
				line = bufferedReader.readLine();
				if(null != line){
					if(Parameters.ENABLE_DEBUG) Log.i(TAG, line);
				}
			}
			while(null != line);
			return true;
		} catch (Exception e) {
			if(Parameters.ENABLE_DEBUG) Log.e(TAG, "command line exception: " + e.getMessage());
			return false;
		}
	}
	
	
	public void upgradeApkByCommandLine(String path){
		String commandLine = "pm install -r " + path;
		if(!runCommandLine(commandLine)){
			if(Parameters.ENABLE_DEBUG) Log.e(TAG, "failed to install apk: " + path);
		}else{
			if(Parameters.ENABLE_DEBUG)
				Log.i(TAG, "installed aplication " + path);
			else Log.i(Parameters.RELEASE_TAG, "Sys Msg 20");
		}
	}
	
	public void uninstallApkByPackage(String appPackage){
		String commandLine = "pm uninstall " + appPackage;
		if(!runCommandLine(commandLine)){
			if(Parameters.ENABLE_DEBUG) Log.e(TAG, "failed to install apk: " + appPackage);
		}else{
			if(Parameters.ENABLE_DEBUG) Log.i(TAG, "uninstalled package " + appPackage);
		}		
	}
	public void enableKeyboard(String kbdPackage){
		String commandLine = "ime enable " + kbdPackage;
		if(!runCommandLine(commandLine)){
			if(Parameters.ENABLE_DEBUG) Log.e(TAG, "failed enable: " + kbdPackage);
		}else{
			if(Parameters.ENABLE_DEBUG) Log.i(TAG, kbdPackage + " enabled");
		}
	}
	public void setDefaultKeyboard(String kbdPackage){
		/*String commandLine = "ime set " + kbdPackage;
		if(!runCommandLine(commandLine)){
			if(Parameters.ENABLE_DEBUG) Log.e(TAG, "failed to set default: " + kbdPackage);
		}else{
			if(Parameters.ENABLE_DEBUG) Log.i(TAG, kbdPackage + " is default");
		}*/
	}
}
