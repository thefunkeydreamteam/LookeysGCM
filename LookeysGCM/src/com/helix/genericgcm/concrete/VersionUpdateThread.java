package com.helix.genericgcm.concrete;

import com.helix.genericgcm.http.RestClient;
import com.helix.genericgcm.protocl.GCMLogic;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.util.Log;

public class VersionUpdateThread extends Thread {
	private static final String TAG = "Lookeys version update thread";
	
	public static final String UPDATER_SUCCESS = "UpdaterS";
	public static final String UPDATER_ATTEMPTS = "Updatera";
	public static final String UPDATER_ATTEMPT_TIME = "Updatert";
	public static final String UPDATER_VERSION = "Updaterv";
	public static final String UPDATER_PASS = "Updaterp";
	public static final String UPDATER_SERVER = "Updatersr";
	public static final String UPDATER_USER = "Updateru";
	public static final String UPDATER_PATH = "Updaterpa";
	public static final String UPDATED_VERSION = "LastUpdatedVersion";
	public static final String SERVICE_NAME_ACTIVATE = "ServiceNameToActivate";
	public static final String DOWNLOAD_LOCK = "UpdateDownloadLock";
	
	
	//private  static final int MAX_ATTEMPTS = 3; // for debug
	private  static int MAX_ATTEMPTS = 3;
	private  static int BACKOFF_SECONDS = 10*60*60; // 10 hours
	//private  static int BACKOFF_SECONDS = 40; // for debug
	
	private Context context;
	private String version;
	private String pathToApp;
	
	private String server;
	private String user;
	private String pass;
	private String serviceName;
	private boolean shouldRetry = false;
	
	public VersionUpdateThread(){
		if(Parameters.ENABLE_DEBUG){
			MAX_ATTEMPTS = 3;
			BACKOFF_SECONDS = 40;
		}
	}
	
	public void Init(Context context, String pathToApp, String version, String server, String user, String pass, String serviceName)
	{	
		this.context = context;
		this.pathToApp = pathToApp;
		this.version = version;
		
		this.server = server;
		this.user = user;
		this.pass = pass;
		this.serviceName = serviceName;
		shouldRetry = false;
		logSuccess();
	}
	
	
	public VersionUpdateThread(Context context){
		if(Parameters.ENABLE_DEBUG){
			MAX_ATTEMPTS = 3;
			BACKOFF_SECONDS = 40;
		}
		this.context = context;
		shouldRetry = false; // LE: must be true, otherwise no first time download 
		SharedPreferences prefs = context.getSharedPreferences(GCMLogic.class.getSimpleName(),
                Context.MODE_PRIVATE);
		int success = prefs.getInt(UPDATER_SUCCESS, 0);
		int attemps = prefs.getInt(UPDATER_ATTEMPTS, 0);
		
		if(Parameters.ENABLE_DEBUG) 
			Log.e(TAG, "VersionUpdateThread attemps = " + attemps + " success = " + success);
		
		this.pathToApp = prefs.getString(UPDATER_PATH, "");
		this.version = prefs.getString(UPDATER_VERSION, "");
		this.serviceName = prefs.getString(SERVICE_NAME_ACTIVATE, "");
		this.server = prefs.getString(UPDATER_SERVER, "");
		this.user = prefs.getString(UPDATER_USER, "");
		this.pass = prefs.getString(UPDATER_PASS, "");
		if(success == 2){
			shouldRetry = false;
			return;
		}
		if(attemps > 0){
			long lastAttempt = prefs.getLong(UPDATER_ATTEMPT_TIME, 0);
			if((lastAttempt == 0) || ((System.currentTimeMillis()/1000 - lastAttempt) < BACKOFF_SECONDS)){
				shouldRetry = false;
				return;
			}
		}
		
		String localVersion = prefs.getString(UPDATED_VERSION, "");
		if(localVersion.equalsIgnoreCase("") || (version.equalsIgnoreCase(""))){
			shouldRetry = false;
			return;
		}

		if(success == 1)
			shouldRetry = true;
		
		if(success == 0)
			shouldRetry = false;
		
	}
	
	public boolean ShouldRetryUpdate(){
		return shouldRetry;
	}
	
	private boolean isDownloadLocked(Context context)
	{
		SharedPreferences prefs = context.getSharedPreferences(GCMLogic.class.getSimpleName(),
                Context.MODE_PRIVATE);
		return(prefs.getBoolean(DOWNLOAD_LOCK, false));
	}
	
	private void LockDownload(Context context)
	{
		SharedPreferences prefs = context.getSharedPreferences(GCMLogic.class.getSimpleName(),
                Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = prefs.edit();
		editor.putBoolean(DOWNLOAD_LOCK, true);
		if(Parameters.ENABLE_DEBUG) 
			Log.e(TAG, "LockDownload");
		editor.commit();
	}
	
	public static void UnlockDownload(Context context)
	{
		SharedPreferences prefs = context.getSharedPreferences(GCMLogic.class.getSimpleName(),
                Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = prefs.edit();
		editor.putBoolean(DOWNLOAD_LOCK, false);
		if(Parameters.ENABLE_DEBUG) 
			Log.w(TAG, "UnlockDownload");
		editor.commit();
	}
	
	@Override
	public void run() 
	{
		if(isDownloadLocked(context)){
			if(Parameters.ENABLE_DEBUG) 
				Log.e(TAG, "download locked");
			return;
		}
		LockDownload(context);
		UpdaterClass updater = new UpdaterClass(context);
		PackageInfo pInfo;
		String localVersion = "";
		boolean installVersion = false;
		
		// LE: reading the local version from prefs so that if the user downgraded, we will not upgrade his version again and again
		SharedPreferences prefs = context.getSharedPreferences(GCMLogic.class.getSimpleName(),
                Context.MODE_PRIVATE);
		localVersion = prefs.getString(UPDATED_VERSION, "");
		if(localVersion.equals("")){
			try {
				pInfo = context.getPackageManager().getPackageInfo(Parameters.LOOKEYS_PACKAGE_NAME, 0);
				localVersion = pInfo.versionName;
			} catch (NameNotFoundException e) {
				if(Parameters.ENABLE_DEBUG) Log.d(TAG, "no local version found");
			}
		}
		if(Parameters.ENABLE_DEBUG) 
			Log.i(TAG, String.format("Server version: %s, local app version: %s",
				version, localVersion));
		else Log.i(Parameters.RELEASE_TAG, "Sys Msg 13 sv" + version + "lv" + localVersion);
		if(updater.shouldUpgradeLocalApp(version, localVersion)){
			installVersion = true;
		}

/*		LE: Original code reading the local version from the package currently installed on the device
 * 			try {
			pInfo = context.getPackageManager().getPackageInfo(Parameters.LOOKEYS_PACKAGE_NAME, 0);
			localVersion = pInfo.versionName;	
			Log.i(TAG, String.format("Server version: %s, local app version: %s",
					version, localVersion));
			if(updater.shouldUpgradeLocalApp(version, localVersion)){
				installVersion = true;
			}
		} catch (NameNotFoundException e) {
			installVersion = true;
			Log.w(TAG, String.format("Could not find package %s, attempting to install it now.",
					Parameters.LOOKEYS_PACKAGE_NAME));
		}*/
		
		if(installVersion){
			if(updater.applyUpdate(pathToApp, server, user, pass)){
				if(Parameters.ENABLE_DEBUG)
					Log.d(TAG, "Upgrade Complete");
				else Log.i(Parameters.RELEASE_TAG, "Sys Msg 14");
				UnlockDownload(context);
				startInstalledApp();
				reportInstall();
		    	logSuccess();
		    	if(Parameters.ENABLE_DEBUG)
					Log.d(TAG, "installVersion");
		    	logUpdated();
		    	return;
		    } 
			UnlockDownload(context);
			startInstalledApp();
			logFail();
		}else{
			if(Parameters.ENABLE_DEBUG) Log.i(TAG, String.format("Current version of app is (%s) and canonical version is (%s). no need to update.",
					localVersion, version));
			if(Parameters.ENABLE_DEBUG)
				Log.d(TAG, "else installVersion");
			logUpdated();
		}
		UnlockDownload(context);
	}
	
	private void reportInstall(){
		String did = getDeviceID();
		if((did == null) || (did.equals(""))){
			if(Parameters.ENABLE_DEBUG)
				Log.d(TAG, "reportInstall device id empty");
			return;
		}
		RestClient rc = new RestClient();
		rc.registerInstallComplete(Parameters.THIRD_PARTY_INSTALL_COMPLETE_URL, serviceName, did);
		if(Parameters.ENABLE_DEBUG)
			Log.d(TAG, "reportInstall sent");
	}
	
	private String getDeviceID(){
		String did = null;
		Encryption encryption = new Encryption();
		String key = "L17$\\h,#l958g";
		// if device id was saved use it
		SharedPreferences prefs = context.getSharedPreferences(GCMLogic.class.getSimpleName(),
                Context.MODE_PRIVATE);
		String eid = prefs.getString(GCMLogic.LOOKEYS_ID_STRING, null);
		if(Parameters.ENABLE_DEBUG) Log.d(TAG, "eid = " + eid);
		if(eid != null){
			did = encryption.decrypt(key, eid);
			if(did != null)
				return did;
		}
		return null;
	}
	
	
	private void startInstalledApp(){
		if((serviceName == null) || (serviceName.equals(""))){
			if(Parameters.ENABLE_DEBUG)
				Log.d(TAG, "startInstalledApp serviceName empty");
			return;
		}
				
		boolean isService = false;
		if(serviceName.startsWith("s_"))
			isService = true;
		else if(serviceName.startsWith("a_"))
			isService = false;
		else {
			if(Parameters.ENABLE_DEBUG)
				Log.d(TAG, "startInstalledApp serviceName is not service nor activity");
			return;
		}
		int ind = serviceName.lastIndexOf('_');
		if(ind < 5){
			if(Parameters.ENABLE_DEBUG)
				Log.d(TAG, "startInstalledApp error in package name");
			return;
		}
		String packageName = serviceName.substring(2, ind).replace("_", ".");
		String className = serviceName.substring(2).replace("_", ".");
		if(Parameters.ENABLE_DEBUG)
			Log.d(TAG, "startInstalledApp packageName = "+ packageName + " className = " + className);
		Intent intent = new Intent();
		intent.setClassName(packageName, className);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		try{
			if(isService)
				context.startService(intent);
			else context.startActivity(intent);
		} catch (Exception e){
			Log.e(TAG, "error in start activity" + e.getMessage());
		}
	}
	
	public void logSuccess(){
		SharedPreferences prefs = context.getSharedPreferences(GCMLogic.class.getSimpleName(),
                Context.MODE_PRIVATE);
		
		String installedVersion = "0.0";
		try {
			PackageInfo pInfo;
			pInfo = context.getPackageManager().getPackageInfo(Parameters.LOOKEYS_PACKAGE_NAME, 0);
			installedVersion = pInfo.versionName;
		} catch (NameNotFoundException e) {
			if(Parameters.ENABLE_DEBUG)
				Log.d(TAG, "Installation of new version failed, could not read info of LOOKEYS_PACKAGE_NAME");
			else Log.i(Parameters.RELEASE_TAG, "Sys Msg 15");
		}
		
		SharedPreferences.Editor editor = prefs.edit();
		editor.putInt(UPDATER_SUCCESS, 0);
		editor.putString(UPDATED_VERSION, installedVersion);
		editor.putInt(UPDATER_ATTEMPTS, 0);
		editor.putString(UPDATER_VERSION, "");
		editor.putString(UPDATER_USER, "");
		editor.putString(UPDATER_PASS, "");
		editor.putString(UPDATER_SERVER, "");
		editor.putString(UPDATER_PATH, "");
		editor.putString(SERVICE_NAME_ACTIVATE, "");
		editor.commit();
		shouldRetry = false;
		
		if(Parameters.ENABLE_DEBUG) 
			Log.e(TAG, "logSuccess success = 0 attempts = 0");
	}
	
	public void logUpdated(){
		SharedPreferences prefs = context.getSharedPreferences(GCMLogic.class.getSimpleName(),
                Context.MODE_PRIVATE);
		if(Parameters.ENABLE_DEBUG)
			Log.d(TAG, "logUpdated");
		SharedPreferences.Editor editor = prefs.edit();
		editor.putLong(Parameters.LOOKEYS_LAST_PULL_TIME, (System.currentTimeMillis()/1000));
		editor.commit();
		shouldRetry = false;
	}
	
	public void logFail(){
		SharedPreferences prefs = context.getSharedPreferences(GCMLogic.class.getSimpleName(),
                Context.MODE_PRIVATE);
		int attemps = prefs.getInt(UPDATER_ATTEMPTS, 0);
		
		SharedPreferences.Editor editor = prefs.edit();
		if(attemps >= MAX_ATTEMPTS){
			editor.putInt(UPDATER_SUCCESS, 2);
			editor.commit();
			if(Parameters.ENABLE_DEBUG)
				Log.d(TAG, "logFail");
			logUpdated();
			if(Parameters.ENABLE_DEBUG) 
				Log.e(TAG, "VersionUpdateThread attemps = " + attemps + " success = 2");
			return;
		}
		editor.putInt(UPDATER_ATTEMPTS, attemps+1);
		editor.putLong(UPDATER_ATTEMPT_TIME, System.currentTimeMillis()/1000);
		if(attemps == 0){
			editor.putInt(UPDATER_SUCCESS, 1);
			editor.putString(UPDATER_VERSION, this.version);
			editor.putString(UPDATER_USER, this.user);
			editor.putString(UPDATER_PASS, this.pass);
			editor.putString(UPDATER_SERVER, this.server);
			editor.putString(UPDATER_PATH, this.pathToApp);
			editor.putString(SERVICE_NAME_ACTIVATE, this.serviceName);
		}
		editor.commit();
		if(Parameters.ENABLE_DEBUG) 
			Log.e(TAG, "VersionUpdateThread attemps = " + attemps+1 + " success = 1");
	}
}