package com.helix.genericgcm.activity;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.helix.genericgcm.protocl.GCMLogic;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;

/**
 * Main UI for the demo app.
 */
public class LookeysGCM extends Activity {
	/*
     * Tag used on log messages.
     */
    static final String TAG = "Lookeys GCM";

    GoogleCloudMessaging gcm;
    SharedPreferences prefs;
    Context context;
    Activity activity;

    String regid;
    GCMLogic gcmLogic;
    
    OnClickListener clearListener = new OnClickListener() {		
		@Override
		public void onClick(View v) {
			gcmLogic.clearRegistrationId(getApplicationContext());				
		}
	};
	OnClickListener registerListener = new OnClickListener() {		
		@Override
		public void onClick(View v) {
			gcmLogic = new GCMLogic(context);				
		}
	};
	
	OnClickListener sendTo3rdPartyServerListener = new OnClickListener() {		
		@Override
		public void onClick(View v) {
		}
	};	
	
	@Override
	public void onWindowFocusChanged (boolean hasFocus){
		super.onWindowFocusChanged(hasFocus);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);   // Do not listen for touch events
    	this.setVisible(false);
    	
	}
	

    @Override
    public void onCreate(Bundle savedInstanceState) {
    	super.onCreate(savedInstanceState);

    	getWindow().addFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);   // Do not listen for touch events
    	this.setVisible(false);
        activity = this;
        context = getApplicationContext();
        gcmLogic = new GCMLogic(context);
    }
}